package fr.afcepf.ai107.boutique.dao;

import java.util.List;

import fr.afcepf.ai107.boutique.entities.Article;

/**
 * Impl�mentation JDBC du dao article.
 */
public class ArticleDAO implements IArticleDAO {

    /**
     * Default constructor.
     */
    public ArticleDAO() {
    }

    /**
     * @return liste des articles.
     */
    public List<Article> getAll() {
        // TODO : A impl�menter...
        return null;
    }

    /**
     * Recherche d'un article par sa r�f�rence.
     * @param ref r�f�rence recherch�e
     * @return article correspondant
     */
    public Article getByRef(String ref) {
        // TODO : A impl�menter
        return null;
    }

    /**
     * Mise � jour du stock.
     * @param idArticle
     * @param quantite nouvelle quantit�
     */
    public void updateStock(int idArticle, int quantite) {
        // TODO : A impl�menter
    }

}