package fr.afcepf.ai107.boutique.entities;

/**
 * Repr�sente une ligne dans le panier.
 */
public class PanierLigne {

    /**
     * @param quantite
     * @param article
     */
    public PanierLigne(int quantite, Article article) {
        this.quantite = quantite;
        this.article = article;
    }

    /**
     * quantit� pour la ligne courante.
     */
    private int quantite;
   
    /**
     * article de la ligne courante.
     */
    private Article article;

    /**
     * @return montant de la ligne (pu x quantite).
     */
    public float getMontant() {
        return quantite * article.getPrixUnitaire();
    }

    /**
     * @return quantite
     */
    public int getQuantite() {
        return quantite;
    }

    /**
     * @param quantite
     */
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /**
     * @return the article
     */
    public Article getArticle() {
        return article;
    }

    /**
     * @param article � ajouter
     */
    public void setArticle(Article article) {
        this.article = article;
    }

}